﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace spider_problem
{
    public class Spider
    {
        public int power { get; set; }
        public int originalIndex { get; set; }
    }
    class Program
    {
        static void Main(string[] args)
        {
            string[] xAndN = Console.ReadLine().Split(' ');
            string[] inputSpiderArray = Console.ReadLine().Split(' ');
            List<Spider> spidersList = convertInputToSpidersList(inputSpiderArray);

            int numToSelect = int.Parse(xAndN[1]);
            int remainingSelections = numToSelect;
            findSpiderWithMaxPower(numToSelect, spidersList);
        }

        private static void findSpiderWithMaxPower(int numToSelect, List<Spider> spidersList)
        {
            int remainingSelections = numToSelect;
            while (remainingSelections-- > 0)
            {
                List<Spider> que = buildQue(spidersList, numToSelect);
                List<Spider> deQue = buildDeQue(spidersList, numToSelect);
                Spider selectedSpider = selectSpider(que);
                Console.Write(selectedSpider.originalIndex.ToString() + " ");
                que.Remove(selectedSpider);
                spidersList = deQue.Concat(que).ToList();
            }
        }
        private static List<Spider> convertInputToSpidersList(string[] inputSpiderArray)
        {
            List<Spider> spidersList = new List<Spider>();
            for (int inputIndex = 0; inputIndex < inputSpiderArray.Length; inputIndex++)
            {
                spidersList.Add(new Spider() { power = int.Parse(inputSpiderArray[inputIndex]), originalIndex = inputIndex + 1 });
            }
            return spidersList;
        }
        private static List<Spider> buildQue(List<Spider> spidersList, int numToSelect)
        {
            if (numToSelect >= spidersList.Count)
            {
                return spidersList;
            }
            return spidersList.GetRange(0, numToSelect);
        }
        private static List<Spider> buildDeQue(List<Spider> spidersList, int numToSelect)
        {
            if (numToSelect >= spidersList.Count)
            {
                return new List<Spider>();
            }
            return spidersList.GetRange(numToSelect, spidersList.Count - numToSelect);
        }
        private static Spider selectSpider(List<Spider> que)
        {
            Spider maxPwrSpider = que[0];
            int currentMaxPower = que[0].power;
            for (int i = 0; i < que.Count; i++)
            {
                Spider spiderInQue = que[i];
                if (spiderInQue.power > currentMaxPower)
                {
                    currentMaxPower = spiderInQue.power;
                    maxPwrSpider = spiderInQue;
                }
                if (spiderInQue.power > 0)
                {
                    spiderInQue.power--;
                }
            }
            return maxPwrSpider;
        }
    }
}

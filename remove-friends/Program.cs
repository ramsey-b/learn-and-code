﻿using System;
using System.Collections.Generic;

namespace remove_friends
{
    class Program
    {
        static void Main(string[] args)
        {
            int numTestcases = int.Parse(Console.ReadLine());
            List<List<int>> allFriendsLists = new List<List<int>>();
            while (numTestcases-- > 0)
            {
                allFriendsLists.Add(createNewFriendsList());
            }
            outputAllFriendsLists(allFriendsLists);
        }
        //creates friends list
        private static List<int> createNewFriendsList()
        {
            String[] inputValues = Console.ReadLine().Split(' ');
            String[] inputFriends = Console.ReadLine().Split(' ');
            int numFriends = int.Parse(inputValues[0]);
            int numToDelete = int.Parse(inputValues[1]);
            List<int> friendsList = createFriendsList(inputFriends);
            return reduceFriendsList(numToDelete, friendsList);
        }

        private static List<int> createFriendsList(string[] inputFriends)
        {
            List<int> friendsList = new List<int>();
            foreach (string friend in inputFriends)
            {
                friendsList.Add(int.Parse(friend));
            }
            return friendsList;
        }

        private static List<int> reduceFriendsList(int numToDelete, List<int> friendsList)
        {
            List<int> newFriendsList = friendsList;
            while (newFriendsList.Count > friendsList.Count - numToDelete)
            {
                deleteFriendsFromList(newFriendsList, friendsList.Count - numToDelete);
            }
            return newFriendsList;
        }

        private static List<int> deleteFriendsFromList(List<int> friendsList, int numToDelete)
        {
            for (int index = 0; index < friendsList.Count - 1 && friendsList.Count > numToDelete; index++)
            {
                int currentFriend = friendsList[index];
                int nextFriend = friendsList[index + 1];
                if (currentFriend < nextFriend)
                {
                    friendsList.Remove(currentFriend);
                }
            }
            return friendsList;
        }

        private static void outputFriendsList(List<int> friendsList)
        {
            string output = "";
            friendsList.ForEach(friend =>
            {
                output += friend + " ";
            });
            Console.WriteLine(output);
        }

        private static void outputAllFriendsLists(List<List<int>> allFriendsLists)
        {
            allFriendsLists.ForEach(friendsList =>
            {
                outputFriendsList(friendsList);
            });
        }
    }
}
